<?php

declare(strict_types=1);

namespace C33s\Bundle\MakerExtraBundle\Maker;

use DateTimeImmutable;
use InvalidArgumentException;
use Symfony\Bundle\MakerBundle\ConsoleStyle;
use Symfony\Bundle\MakerBundle\DependencyBuilder;
use Symfony\Bundle\MakerBundle\Generator;
use Symfony\Bundle\MakerBundle\InputConfiguration;
use Symfony\Bundle\MakerBundle\Maker\AbstractMaker;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;

class ContentMaker extends AbstractMaker
{
    public static function getCommandName(): string
    {
        return 'make:content';
    }

    public static function getCommandDescription(): string
    {
        return 'Creates a new console command class';
    }

    //phpcs:disable Generic.CodeAnalysis.UnusedFunctionParameter.FoundInExtendedClassAfterLastUsed
    public function configureCommand(Command $command, InputConfiguration $inputConfig): void
    {
        $exampleClass = Str::asClassName(Str::getRandomTerm());
        $nameQuery = sprintf('Choose a name for your Bundle (e.g. <fg=yellow>%s</>)', $exampleClass);
        $command
            ->addArgument('name', InputArgument::OPTIONAL, $nameQuery)
            ->addOption('path', null, InputOption::VALUE_REQUIRED, '', 'src/App/DataContent')
            ->addOption('skeleton-dir', null, InputOption::VALUE_REQUIRED, '', __DIR__.'/../../skeleton')
        ;
    }

    //phpcs:enable

    public function generate(InputInterface $input, ConsoleStyle $io, Generator $generator): void
    {
        $io->text('make content');
        $skeletonDir = $this->normalizeParameter($input->getOption('skeleton-dir'));
        $name = $this->normalizeParameter($input->getArgument('name'));
        $date = (new DateTimeImmutable())->format('YmdHis');
        $classDetails = $generator->createClassNameDetails("Content$date$name", 'DataContent', '');
        $generator->generateClass(
            $classDetails->getFullName(),
            $skeletonDir.'/Content.tpl.php',
        );
        $generator->writeChanges();
        $this->writeSuccessMessage($io);
    }

    /**
     * @param mixed $parameter
     */
    private function normalizeParameter($parameter): string
    {
        if (!is_string($parameter)) {
            throw new InvalidArgumentException('Invalid name, must be a string. got "'.gettype($parameter).'"');
        }

        return trim($parameter);
    }

    public function configureDependencies(DependencyBuilder $dependencies): void
    {
    }
}
